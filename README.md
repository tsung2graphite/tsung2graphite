tsung2graphite
==============

tsung2graphit.py reads a tsunglog mad with option backend=json and
send dats to a [graphite server](http://graphite.wikidot.com/)

Tests
=====

To run test using nose and coverage

$ nosetests tests.py --with-coverage --cover-package=tsung2graphite
