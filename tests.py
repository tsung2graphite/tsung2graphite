#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
tsung2graphite.py reads tsung's log in json format, export datas to graphite
server.
Your tsung scenario must begin with option backend="json" to force 
tsung dumping log data in json format instead of tsung native format :

<tsung loglevel="notice" dumptraffic="false" version="1.0" backend="json">

"""
import random
import unittest
import tsung2graphite as t2g
import json
import tempfile

class TestSequenceFunctions(unittest.TestCase):

    def setUp(self):
        self.datas = """{
 "stats": [
 {"timestamp": 1364200887,  "samples": []},
 {"timestamp": 1364200897,  "samples": [   {"name": "users", "value": 0, "max": 0}, {"name": "users_count", "value": 0, "total": 0}, {"name": "finish_users_count", "value": 0, "total": 0}]},
 {"timestamp": 1364200907,  "samples": [   {"name": "users", "value": 5, "max": 5}, {"name": "session", "value": 1, "mean": 251.31494140625,"stdvar": 0,"max":  251.31494140625,"min": 251.31494140625 ,"global_mean": 0 ,"global_count": 0}, {"name": "users_count", "value": 6, "total": 6}, {"name": "finish_users_count", "value": 1, "total": 1}, {"name": "request", "value": 10, "mean": 261.2021728515625,"stdvar": 166.67755491994515,"max":  555.732177734375,"min": 70.555908203125 ,"global_mean": 0 ,"global_count": 0}, {"name": "page", "value": 10, "mean": 261.2021728515625,"stdvar": 166.67755491994515,"max":  555.732177734375,"min": 70.555908203125 ,"global_mean": 0 ,"global_count": 0}, {"name": "connect", "value": 6, "mean": 93.53495279947917,"stdvar": 40.347040554965105,"max":  170.2119140625,"min": 56.39697265625 ,"global_mean": 0 ,"global_count": 0}, {"name": "tr_index_request", "value": 1, "mean": 249.35107421875,"stdvar": 0,"max":  249.35107421875,"min": 249.35107421875 ,"global_mean": 0 ,"global_count": 0}, {"name": "http_200", "value": 10, "total": 10}, {"name": "size_rcv", "value": 66794, "total": 66794}, {"name": "size_sent", "value": 986, "total": 986}, {"name": "connected", "value": 5, "max": 5}]},
 {"timestamp": 1364200917,  "samples": [   {"name": "users", "value": 4, "max": 5}, {"name": "session", "value": 3, "mean": 8032.954264322917,"stdvar": 5449.750285346897,"max":  12180.706787109375,"min": 251.31494140625 ,"global_mean": 251.31494140625 ,"global_count": 1}, {"name": "users_count", "value": 2, "total": 8}, {"name": "finish_users_count", "value": 3, "total": 4}, {"name": "request", "value": 15, "mean": 117.5943359375,"stdvar": 62.869878200062416,"max":  555.732177734375,"min": 41.867919921875 ,"global_mean": 261.2021728515625 ,"global_count": 10}, {"name": "page", "value": 15, "mean": 117.5943359375,"stdvar": 62.869878200062416,"max":  555.732177734375,"min": 41.867919921875 ,"global_mean": 261.2021728515625 ,"global_count": 10}, {"name": "connect", "value": 1, "mean": 64.247802734375,"stdvar": 0.0,"max":  170.2119140625,"min": 56.39697265625 ,"global_mean": 93.53495279947917 ,"global_count": 6}, {"name": "tr_index_request", "value": 1, "mean": 331.77490234375,"stdvar": 0.0,"max":  331.77490234375,"min": 249.35107421875 ,"global_mean": 249.35107421875 ,"global_count": 1}, {"name": "http_200", "value": 25, "total": 25}, {"name": "size_rcv", "value": 90991, "total": 157785}, {"name": "size_sent", "value": 1954, "total": 2940}, {"name": "connected", "value": -2, "max": 3}]}]}
"""

    def test_prepare(self):
        # make sure the shuffled sequence does not lose any elements
        nbelem = len(t2g.prepare_datas(json.loads(self.datas), 'foobar'))
        self.assertEqual(nbelem, 104)

    def test_append_data(self):
        value = {"name": "size_rcv", "value": 90991, "max": 157785}
        stat = {"timestamp": 1364200897, "samples": [ {"name": "users", "value": 0, "max": 0}, {"name": "users_count", "value": 0, "total": 0}]}

        attend = "foo.tsung.size_rcv.max 157785 1364200897\n"
        
        line = t2g.append_data('foo', value, stat, 'max')

        self.assertEqual(line, attend)

    def test_parse_file(self):
        """
        Test the parse_file that must returns a json object
        """
        tmpf = tempfile.NamedTemporaryFile(delete=False)
        tmp_path = tmpf.name
        tmpf.write(self.datas)
        tmpf.close()

        result = t2g.parse_file(tmp_path)
        self.assertEqual(result, json.loads(self.datas))

    def test_manage(self):
        start, stop = t2g.manage(json.loads(self.datas))
        self.assertEqual(start, 1364200887)
        self.assertEqual(stop, 1364200917)

if __name__ == '__main__':
    unittest.main()
